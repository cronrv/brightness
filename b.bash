#!/bin/bash

maxB=$(cat /sys/class/backlight/intel_backlight/max_brightness);

echo "Max_Brightness: $maxB"

if [[ $1 -gt 0 ]] && [[ $1 -lt $maxB ]]
then
	bash -c "echo $1 > /sys/class/backlight/intel_backlight/brightness" 2>/dev/null;
fi;

if [[ $? -eq 1 ]]
then
	echo -e "\033[0;31mRoot permissions needed:\033[0m In order to edit file '/sys/class/backlight/intel_backlight/brightness'";
	exit 1;
fi;

currentB=$(cat /sys/class/backlight/intel_backlight/brightness);

echo -e "\033[0;32mBrightness:\033[0m $currentB"

exit 0;
#echo $1 >> 

cat /sys/class/backlight/intel_backlight/actual_brightness
